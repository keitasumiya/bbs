﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using UnityEngine;

public class FileIO
{
	static string FilePathRoot = Application.temporaryCachePath;

	public static void WriteBytes(string out_path, byte[] binary)
	{
		try
		{
			// Debug.Log(FilePathRoot);
			FileInfo fi = new FileInfo(Path.Combine(FilePathRoot, out_path));
			using (FileStream fs = fi.Create())
			{
				fs.Write(binary, 0, binary.Length);
				fs.Close();
			}
		}
		catch (System.Exception ex)
		{
			Debug.LogError(ex.ToString());
		}
	}
	
	public static void WriteText(string out_path, string text)
	{
		try
		{
			FileInfo fi = new FileInfo(Path.Combine(FilePathRoot, out_path));
			using (StreamWriter sw = fi.CreateText())
			{
				sw.Write(text);
				sw.Close();
			}
		}
		catch (System.Exception ex)
		{
			Debug.LogError(ex.ToString());
		}
	}

	public static void WriteTexture(string out_path, Texture2D tex)
	{
		try
		{
			byte[] data = tex.EncodeToJPG();
			WriteBytes(out_path, data);
		}
		catch (System.Exception ex)
		{
			Debug.LogError(ex.ToString());
		}
	}

	// ===========================================================================

	public static void ReadBytes(string path, Action<byte[]> callback)
	{
		Debug.Assert(callback != null);

		try
		{
			string filepath = Path.Combine(FilePathRoot, path);
			byte[] data = File.ReadAllBytes(filepath);
			callback(data);
		}
		catch (System.Exception ex)
		{
			Debug.LogError(ex.ToString());
		}
	}

	public static void ReadText(string path, Action<string, string> callback)
	{
		Debug.Assert(callback != null);

		try
		{
			FileInfo fi = new FileInfo(Path.Combine(FilePathRoot, path));
			using (StreamReader s = new StreamReader(fi.OpenRead(), Encoding.UTF8))
			{
				callback(null, s.ReadToEnd());
				s.Close();
			}
		}
		catch (System.Exception ex)
		{
			Debug.LogError(ex.ToString());
			callback(ex.ToString(), null);
		}
	}

	public static void ReadTexture(string path, Action<Texture2D> callback)
	{
		Debug.Assert(callback != null);

		ReadBytes(path, (data) => {
			try
			{
				Texture2D tex = new Texture2D(2, 2);
				tex.LoadImage(data);
				callback(tex);
			}
			catch (System.Exception ex)
			{
				Debug.LogError(ex.ToString());
			}
		});
	}

	public static bool Exists(string path)
	{
		FileInfo fi = new FileInfo(Path.Combine(FilePathRoot, path));
		return fi.Exists;
	}
}