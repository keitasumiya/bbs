﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

using UnityEngine;

/*

Usage:

HTTP.Get("http://localhost:3000/test_static_file.txt", (err, www) => {
	Debug.Log(err);

	Debug.Log(www.text);
	Debug.Log(www.texture);
	Debug.Log(www.bytes);
});

*/

public class HTTP : MonoBehaviour
{
	private HTTP() { }

	private static HTTP _instance;
	static HTTP Instance
	{
		get
		{
			if (_instance == null)
			{
				GameObject go = new GameObject("HTTPSingleton");
				_instance = go.AddComponent<HTTP>();
			}
			return _instance;
		}
	}

	public static void GetText(string url, string id, Action<string, string> onResult, bool forceUpdate = true)
	{
		Debug.Log(url);
		if(AppSettings.USE_RESOURCES_DATA){
    			TextAsset ta = Resources.Load(id) as TextAsset;
				onResult(null, ta.text);
		}
		else if (FileIO.Exists(id) && forceUpdate == false)
		{
			FileIO.ReadText(id, (err, text) => {
				onResult(err, text);
			});
		}
		else
		{
			Get(url, (err, www) => {
				if (err != null)
				{
					Debug.Log("Getting [" + url + "] failed. using cacshe");
					FileIO.ReadText(id, (err2, text) => {
						if(err2 != null){
							onResult(err + " " + err2, text);
						}else{
							onResult(null, text );
						}
					}
					);
				}
				else
				{
					FileIO.WriteText(id, www.text);
					onResult(null, www.text);
				}
			});
		}
	}
	public static void GetTexture(string url, string id, Action<string, Texture2D> onResult, bool forceUpdate = true)
	{
		if ( FileIO.Exists(id)
			&& forceUpdate == false)
		{
			FileIO.ReadTexture(id, (tex) =>
			{
				onResult(null, tex);
			});
		}
		else
		{
			Get(url, (err, www) =>
			{
				if (err != null)
				{
					onResult(err, null);
				}
				else
				{
					FileIO.WriteTexture(id, www.texture);
					onResult(null, www.texture);
				}
			});
		}
	}

	public static void Get(string url, Action<string, WWW> onResult)
	{
		Debug.Log("get " + url);
		Instance.StartCoroutine(Instance._Get(url, onResult));
	}

	IEnumerator _Get(string url, Action<string, WWW> onResult)
	{
		WWW www = new WWW(url);
		www.threadPriority = ThreadPriority.Low;

		yield return www;

		Debug.Assert(onResult != null);
		onResult(www.error, www);
	}


	public static void Post(string url, string data, Action<string, WWW> onResult)
	{
		Instance.StartCoroutine(Instance._Post(url, data, onResult));
	}

	IEnumerator _Post(string url, string data, Action<string, WWW> onResult)
	{
		Dictionary<string, string> header = new Dictionary<string,string>();
        header.Add ("Content-Type", "application/json; charset=UTF-8");
        byte[] postBytes = Encoding.Default.GetBytes(data);
        WWW www = new WWW (url, postBytes, header);
		www.threadPriority = ThreadPriority.Low;
        yield return www;

        // 成功
        if (www.error == null) {
            Debug.Log("Post Success");
        }else{
            Debug.Log("Post Failure");          
        }
		
		Debug.Assert(onResult != null);
		onResult(www.error, www);
	}


}