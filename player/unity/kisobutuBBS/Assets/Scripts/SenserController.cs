﻿using System.Collections;
using UnityEngine.Networking;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class SenserController : MonoBehaviour
{

    private float span = 3f;
    private float currentTime = 0f;

    public string sensorName = "name";
    public string sensorIP = "192.168.100.150";
    public string sensorPlace = "place";
    private string sensorJsonStr;
    private GameObject eachDataPanel;


    void Start()
    {
        this.transform.Find("PlacePanel/Text").GetComponent<Text>().text = sensorPlace;
        GetSensorDataViaHttp();
    }

    void Update()
    {
        currentTime += Time.deltaTime;

        if (currentTime > span)
        {
            Debug.LogFormat("{0}秒経過", span);
            GetSensorDataViaHttp();
            Debug.Log(sensorJsonStr);
            LitJson.JsonData sensorJsonData = LitJson.JsonMapper.ToObject(sensorJsonStr);
            this.transform.Find("TemperaturePanel/Text").GetComponent<Text>().text = float.Parse(sensorJsonData["temperature"].ToString()).ToString("f1");
            this.transform.Find("HumidityPanel/Text").GetComponent<Text>().text = float.Parse(sensorJsonData["humidity"].ToString()).ToString("f1");
            this.transform.Find("PressurePanel/Text").GetComponent<Text>().text = float.Parse(sensorJsonData["pressure"].ToString()).ToString("f1");

            currentTime = 0f;
        }
    }


    void GetSensorDataViaHttp()
    {
        string url = "http://"+ sensorIP +"/data";
        HTTP.Get(url, (err, www) =>
        {
            if (err != null)
            {
                Debug.Log(err);
            }
            else
            {
                sensorJsonStr = www.text.ToString().Replace("_", "\"");
                Debug.Log(sensorJsonStr);
            }
        }
        );
    }


}
