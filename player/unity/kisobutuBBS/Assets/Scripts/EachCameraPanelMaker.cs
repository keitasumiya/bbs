﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class EachCameraPanelMaker : MonoBehaviour
{
    public GameObject CameraPanel;
    public GameObject EachCameraPanelPrefab;
    private LitJson.JsonData devicesJson;


    void Start()
    {
        devicesJson = readDevicesJson();
        Debug.Log(devicesJson["camera"].Count);

        for (int i = 0; i < devicesJson["camera"].Count; i++)
        {
            string cameraName = devicesJson["camera"][i]["name"].ToString();
            string cameraIp = devicesJson["camera"][i]["ip"].ToString();
            string cameraPlace = devicesJson["camera"][i]["place"].ToString();
            GameObject EachCameraPanelTmp = Instantiate(EachCameraPanelPrefab, CameraPanel.transform) as GameObject;
            EachCameraPanelTmp.transform.Find("CameraLabel").GetComponent<Text>().text = cameraName + " @" + cameraPlace;
            EachCameraPanelTmp.transform.Find("CameraQuad").GetComponent<MJStreamingPlayer>().serverName = cameraName;
            EachCameraPanelTmp.transform.Find("CameraQuad").GetComponent<MJStreamingPlayer>().serverUrl = "http://" + cameraIp + ":8080/?action=stream";
            EachCameraPanelTmp.transform.Find("CameraQuad").GetComponent<MJStreamingPlayer>().serverPlace = cameraPlace;
        }

    }



    // Sub functions : Read data ################################################################################################
    // =====================================================================================================
    LitJson.JsonData readDevicesJson()
    {
        string path = AppSettings.WHOLE_DIR + AppSettings.SETTINGS_DIR + AppSettings.DEV_FILENAME;

        string jsonStr = File.ReadAllText(path);

        //Debug.Log(jsonStr);
        LitJson.JsonData jsonData = LitJson.JsonMapper.ToObject(jsonStr);

        return jsonData;
    }

}
