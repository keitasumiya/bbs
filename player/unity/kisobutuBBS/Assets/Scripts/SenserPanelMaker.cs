﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class SenserPanelMaker : MonoBehaviour
{
    public GameObject EnvDataPanel;
    public GameObject EnvEachDataPanelPrefab;
    private LitJson.JsonData devicesJson;
    
    // Start is called before the first frame update
    void Start()
    {
        devicesJson = readDevicesJson();
        //Debug.Log(devicesJson["sensor"][0]["name"]);
        Debug.Log(devicesJson["sensor"].Count);

        Vector2 cellsize = EnvDataPanel.GetComponent<GridLayoutGroup>().cellSize;
        cellsize.y = EnvDataPanel.GetComponent<RectTransform>().sizeDelta.y / devicesJson["sensor"].Count;
        float cellSizeY = cellsize.y;
        EnvDataPanel.GetComponent<GridLayoutGroup>().cellSize = cellsize;

        for (int i = 0; i < devicesJson["sensor"].Count; i++)
        {
            GameObject EnvEachDataPanelTmp = Instantiate(EnvEachDataPanelPrefab, EnvDataPanel.transform) as GameObject;
            Vector2 cellsizeTmp = EnvEachDataPanelTmp.GetComponent<GridLayoutGroup>().cellSize;
            cellsizeTmp.y = cellSizeY;
            EnvEachDataPanelTmp.GetComponent<GridLayoutGroup>().cellSize = cellsizeTmp;

            EnvEachDataPanelTmp.GetComponent<SenserController>().sensorName = devicesJson["sensor"][i]["name"].ToString();
            EnvEachDataPanelTmp.GetComponent<SenserController>().sensorIP = devicesJson["sensor"][i]["ip"].ToString();
            EnvEachDataPanelTmp.GetComponent<SenserController>().sensorPlace = devicesJson["sensor"][i]["place"].ToString();
        }

    }


    // Sub functions : Read data ################################################################################################
    // =====================================================================================================
    LitJson.JsonData readDevicesJson()
    {
        string path = AppSettings.WHOLE_DIR + AppSettings.SETTINGS_DIR + AppSettings.DEV_FILENAME;

        string jsonStr = File.ReadAllText(path);

        //Debug.Log(jsonStr);
        LitJson.JsonData jsonData = LitJson.JsonMapper.ToObject(jsonStr);

        return jsonData;
    }

}
