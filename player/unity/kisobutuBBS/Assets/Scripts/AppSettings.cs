﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppSettings {
    public static string WHOLE_DIR = "C:/kisobutu/bbs/";
    public static string SETTINGS_DIR = "settings/";
    public static string SQL_DATA_DIR = "sql/data/";
    public static string DEV_FILENAME = "devices.json";
    public static string EXP_THEME_FILENAME = "experiments-themes.json";
    public static string COMP_FILENAME = "completions.csv";
    public static string SQL_SRC_DIR = "sql/src/";
    public static string SQL_BAT_FILENAME = "count_completions.bat";

    //   public static string DATA_DIR = PlayerPrefs.GetString("key_data_dir", "D:/sbsc/data");
    //   public static string JSON_DIR = PlayerPrefs.GetString("key_json_dir", "json/");
    //   public static string IMAGE_DIR = PlayerPrefs.GetString("key_image_dir", "image/");
    //   public static string MOVIE_DIR = PlayerPrefs.GetString("key_movie_dir", "movie/");
    //   public static string THUMBNAIL_DIR = PlayerPrefs.GetString("key_thumbnail_dir", "thumbnail/");


    //   public static string TABLET_HOST = PlayerPrefs.GetString("key_tablet_ip", "192.168.10.206"); // kikkoman-ipad in kikkoman
    //   public static int PLAYER2TABLET_OSC_PORT = 8001;
    //   public static int TABLET2PLAYER_OSC_PORT = 8002;

    //   public static string SERVER_HOST = PlayerPrefs.GetString("key_ip", "192.168.10.200"); // HP Z8 G4 WKS in kikkoman
    //   public static int SERVER_HOST_OSC_PORT = 8000; // HP Z8 G4 WKS


    //   public static string SERVER_REMOTE_HOST = PlayerPrefs.GetString("key_rempte_ip", "192.168.55.223");


    //public static int FORCE_UPDATE = PlayerPrefs.GetInt("key_force_update", 1);
    public static bool USE_RESOURCES_DATA = true;

	//public static string CAMERA_SERVER_HOST = PlayerPrefs.GetString("key_camera_ip", "192.168.10.30");	
	//public static string CAMERA_SERVER2_HOST = PlayerPrefs.GetString("key_camera2_ip", "192.168.10.31");

	//public static int CAMERA_SERVER_PORT = 10000;
	//public static int CAMERA_INPUT_PORT = 10001;
	//public static string getUrl(){
	//	string url = "http://" + SERVER_HOST + ":3000/";
	//	return url;
	//}
	//public static string getCarCameraServerUrl(){
	//	string target_url = "http://" + CAMERA_SERVER_HOST + ":7000/";
	//	string url = "http://" + SERVER_HOST + ":3000/redirect?dest=" + target_url;
	//	return url;
	//}
	//public static string getCarCameraServer2Url(){
	//	string target_url = "http://" + CAMERA_SERVER2_HOST + ":7000/";
	//	string url = "http://" + SERVER_HOST + ":3000/redirect?dest=" + target_url;

	//	return url;
	//}
	//public static string getRemoteUrl(){
	//	string url = "http://" + SERVER_REMOTE_HOST + ":3000/";
	//	return url;
	//}
	//public static string getRemoteCameraServerUrl(){
	//	string url = "http://" + SERVER_REMOTE_HOST + ":7000/";
	//	return url;
	//}
}
