﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine.UI;
using UnityEngine;
using System.Text;
using System.IO;


public class CompletionController : MonoBehaviour
{
    // Variables ###########################################################################################
    // Public ==============================================================================================

    // Private ==============================================================================================
    private GameObject CompletionUpdateTimePanel;
    private GameObject dataPanel1st;
    private GameObject dataPanel2nd;
    private GameObject tabToggle1st;
    private GameObject tabToggle2nd;
    private int currentMonth = System.DateTime.Now.Month;
    private GameObject dataPanel11;
    private GameObject dataPanel12;
    private GameObject dataPanel13;
    private GameObject dataPanel14;
    private GameObject dataPanel15;
    private GameObject dataPanel16;
    private GameObject dataPanel21;
    private GameObject dataPanel22;
    private GameObject dataPanel23;
    private GameObject dataPanel24;
    private GameObject dataPanel25;
    private GameObject dataPanel26;
    private List<string[]> completionCsvData = new List<string[]>();
    private LitJson.JsonData experimentsThemes;
    private Dictionary<string, string> experimentsComplitions = new Dictionary<string, string>();
    private Process process;
    private float span = 1f;
    private float currentTime = 0f;



    // Main functions ################################################################################################
    // =====================================================================================================
    private void Awake()
    {
        CompletionUpdateTimePanel = this.transform.Find("LabelPanel/TimePanel").gameObject;
        dataPanel1st = this.transform.Find("DataPanel1st").gameObject;
        dataPanel2nd = this.transform.Find("DataPanel2nd").gameObject;
        tabToggle1st = this.transform.Find("TabContainerPanel/TabToggle1st").gameObject;
        tabToggle2nd = this.transform.Find("TabContainerPanel/TabToggle2nd").gameObject;
        dataPanel11 = this.transform.Find("DataPanel1st/DataPanel1").gameObject;
        dataPanel12 = this.transform.Find("DataPanel1st/DataPanel2").gameObject;
        dataPanel13 = this.transform.Find("DataPanel1st/DataPanel3").gameObject;
        dataPanel14 = this.transform.Find("DataPanel1st/DataPanel4").gameObject;
        dataPanel15 = this.transform.Find("DataPanel1st/DataPanel5").gameObject;
        dataPanel16 = this.transform.Find("DataPanel1st/DataPanel6").gameObject;
        dataPanel21 = this.transform.Find("DataPanel2nd/DataPanel1").gameObject;
        dataPanel22 = this.transform.Find("DataPanel2nd/DataPanel2").gameObject;
        dataPanel23 = this.transform.Find("DataPanel2nd/DataPanel3").gameObject;
        dataPanel24 = this.transform.Find("DataPanel2nd/DataPanel4").gameObject;
        dataPanel25 = this.transform.Find("DataPanel2nd/DataPanel5").gameObject;
        dataPanel26 = this.transform.Find("DataPanel2nd/DataPanel6").gameObject;
    }


    // =====================================================================================================
    void Start()
    {
        experimentsThemes = readExperimentsThemesJson();
        //Debug.Log(experimentsThemes["2"]);

        updateCompletionLabel();

        readCompletionCsv();

        makeExperimentsComplitions();
        //Debug.Log(experimentsComplitions["02 体積の測定"]);

        displayCurrentCompletionPanel();

        //callBatFile(AppSettings.WHOLE_DIR + "sql/src/count_completions_test.bat");
    }


    // =====================================================================================================
    void Update()
    {
        currentTime += Time.deltaTime;
        //UnityEngine.Debug.Log(currentTime);

        if (currentTime > span)
        {
            UnityEngine.Debug.LogFormat("{0}秒経過!!", span);
            //Debug.Log("n秒経過");

            readCompletionCsv();
            //Debug.Log(completionCsvData[0][1]);

            updateExperimentsComplitions();
            //Debug.Log(experimentsComplitions["02 体積の測定"]);

            updateCompletionNum();

            callBatFile(AppSettings.WHOLE_DIR + AppSettings.SQL_SRC_DIR + AppSettings.SQL_BAT_FILENAME);

            currentTime = 0f;
        }
    }



    // Sub functions : Time ################################################################################################
    // =====================================================================================================
    void updateUpdatingTime()
    {
        string currentHour = System.DateTime.Now.Hour.ToString("D2");
        string currentMinute = System.DateTime.Now.Minute.ToString("D2");
        CompletionUpdateTimePanel.transform.Find("Text").GetComponent<Text>().text = "更新時刻 " + currentHour + ":" + currentMinute;
    }



    // Sub functions : Read data ################################################################################################
    // =====================================================================================================
    LitJson.JsonData readExperimentsThemesJson()
    {
        string path = AppSettings.WHOLE_DIR + AppSettings.SETTINGS_DIR + AppSettings.EXP_THEME_FILENAME;

        string jsonStr = File.ReadAllText(path);

        //Debug.Log(jsonStr);
        LitJson.JsonData jsonData = LitJson.JsonMapper.ToObject(jsonStr);

        return jsonData;
    }


    // =====================================================================================================
    void readCompletionCsv()
    {
        completionCsvData.Clear();
        string path = AppSettings.WHOLE_DIR + AppSettings.SQL_DATA_DIR + AppSettings.COMP_FILENAME;

        var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
        var sr = new StreamReader(fs, Encoding.UTF8);
        for (int i = 0; i < 12; i++)
        {
            string line = sr.ReadLine();
            completionCsvData.Add(line.Split(','));

            //Debug.Log(completionCsvData[i][0]);
            //Debug.Log(completionCsvData[i][1]);
        }

    }



    // Sub functions : Experiments Complitions Dictionary ################################################################################################
    // =====================================================================================================
    void makeExperimentsComplitions()
    {
        for (int i = 2; i <= 13; i++)
        {
            string iStr = i.ToString();
            int iMinus2 = i - 2;
            experimentsComplitions.Add(experimentsThemes[iStr].ToString(), completionCsvData[iMinus2][1]);
        }
    }


    // =====================================================================================================
    void updateExperimentsComplitions()
    {
        for (int i = 2; i <= 13; i++)
        {
            string iStr = i.ToString();
            int iMinus2 = i - 2;
            experimentsComplitions[experimentsThemes[iStr].ToString()] = completionCsvData[iMinus2][1];
        }
    }



    // Sub functions : Display Complitions ################################################################################################
    // =====================================================================================================
    void updateCompletionLabel()
    {
        updateUpdatingTime();
        dataPanel11.transform.Find("LabelPanel/Text").GetComponent<Text>().text = experimentsThemes["2"].ToString();
        dataPanel12.transform.Find("LabelPanel/Text").GetComponent<Text>().text = experimentsThemes["3"].ToString();
        dataPanel13.transform.Find("LabelPanel/Text").GetComponent<Text>().text = experimentsThemes["4"].ToString();
        dataPanel14.transform.Find("LabelPanel/Text").GetComponent<Text>().text = experimentsThemes["5"].ToString();
        dataPanel15.transform.Find("LabelPanel/Text").GetComponent<Text>().text = experimentsThemes["6"].ToString();
        dataPanel16.transform.Find("LabelPanel/Text").GetComponent<Text>().text = experimentsThemes["7"].ToString();
        dataPanel21.transform.Find("LabelPanel/Text").GetComponent<Text>().text = experimentsThemes["8"].ToString();
        dataPanel22.transform.Find("LabelPanel/Text").GetComponent<Text>().text = experimentsThemes["9"].ToString();
        dataPanel23.transform.Find("LabelPanel/Text").GetComponent<Text>().text = experimentsThemes["10"].ToString();
        dataPanel24.transform.Find("LabelPanel/Text").GetComponent<Text>().text = experimentsThemes["11"].ToString();
        dataPanel25.transform.Find("LabelPanel/Text").GetComponent<Text>().text = experimentsThemes["12"].ToString();
        dataPanel26.transform.Find("LabelPanel/Text").GetComponent<Text>().text = experimentsThemes["13"].ToString();
    }


    // =====================================================================================================
    void updateCompletionNum()
    {
        updateUpdatingTime();
        dataPanel11.transform.Find("NumPanel/Text").GetComponent<Text>().text = experimentsComplitions[experimentsThemes["2"].ToString()];
        dataPanel12.transform.Find("NumPanel/Text").GetComponent<Text>().text = experimentsComplitions[experimentsThemes["3"].ToString()];
        dataPanel13.transform.Find("NumPanel/Text").GetComponent<Text>().text = experimentsComplitions[experimentsThemes["4"].ToString()];
        dataPanel14.transform.Find("NumPanel/Text").GetComponent<Text>().text = experimentsComplitions[experimentsThemes["5"].ToString()];
        dataPanel15.transform.Find("NumPanel/Text").GetComponent<Text>().text = experimentsComplitions[experimentsThemes["6"].ToString()];
        dataPanel16.transform.Find("NumPanel/Text").GetComponent<Text>().text = experimentsComplitions[experimentsThemes["7"].ToString()];
        dataPanel21.transform.Find("NumPanel/Text").GetComponent<Text>().text = experimentsComplitions[experimentsThemes["8"].ToString()];
        dataPanel22.transform.Find("NumPanel/Text").GetComponent<Text>().text = experimentsComplitions[experimentsThemes["9"].ToString()];
        dataPanel23.transform.Find("NumPanel/Text").GetComponent<Text>().text = experimentsComplitions[experimentsThemes["10"].ToString()];
        dataPanel24.transform.Find("NumPanel/Text").GetComponent<Text>().text = experimentsComplitions[experimentsThemes["11"].ToString()];
        dataPanel25.transform.Find("NumPanel/Text").GetComponent<Text>().text = experimentsComplitions[experimentsThemes["12"].ToString()];
        dataPanel26.transform.Find("NumPanel/Text").GetComponent<Text>().text = experimentsComplitions[experimentsThemes["13"].ToString()];
    }


    // =====================================================================================================
    void displayCurrentCompletionPanel()
    {
        if (currentMonth >= 4 && currentMonth <= 8)
        {
            dataPanel1st.SetActive(true);
            dataPanel2nd.SetActive(false);
            tabToggle1st.GetComponent<Toggle>().isOn = true;
            tabToggle2nd.GetComponent<Toggle>().isOn = false;
        }
        else
        {
            dataPanel1st.SetActive(false);
            dataPanel2nd.SetActive(true);
            tabToggle1st.GetComponent<Toggle>().isOn = false;
            tabToggle2nd.GetComponent<Toggle>().isOn = true;
        }
    }



    private void callBatFile(string batFilePath)
    {
        // 他のプロセスが実行しているなら行わない
        if (process != null) return;

        // 新規プロセスを作成し、batファイルのパスを登録
        process = new Process();
        //process.StartInfo.FileName = scriptPath + batFilePath;
        process.StartInfo.FileName = batFilePath;
        //process.StartInfo.FileName = System.Environment.GetEnvironmentVariable("ComSpec");
        process.StartInfo.CreateNoWindow = true;
        process.StartInfo.UseShellExecute = false;

        //process.StartInfo.UseShellExecute = false;
        //process.StartInfo.RedirectStandardOutput = true;
        //process.StartInfo.RedirectStandardInput = false;
        ////ウィンドウを表示しないようにする
        //process.StartInfo.CreateNoWindow = true;
        //コマンドラインを指定（"/c"は実行後閉じるために必要）
        //process.StartInfo.Arguments = @"/c dir C:\kisobutu\bbs\sql\src\count_completions.sql > C:\kisobutu\bbs\sql\src\test.txt /w";
        //process.StartInfo.Arguments = @"/c C:\kisobutu\bbs\sql\src\count_completions_test.bat /w";

        // 外部プロセスの終了を検知するための設定
        process.EnableRaisingEvents = true;
        process.Exited += process_Exited;

        // 外部プロセスを実行
        process.Start();

        //process.WaitForExit();
        //process.Close();
        //process.Dispose();
        //process = null;
    }

    // 外部プロセスの終了を検知してプロセスを終了
    void process_Exited(object sender, System.EventArgs e)
    {
        process.Dispose();
        process = null;
    }
}
