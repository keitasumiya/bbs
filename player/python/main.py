#codeing utf-8
import tkinter as tk
import json
import io

def addTempHumidGrid(rowNum, dataList):
    _fw = frameTempHumid.winfo_width()
    _fh = frameTempHumid.winfo_height()

    print( int(_fw/3) )
    tk.Label(frameTempHumid, 
        text = dataList[0], 
        width = 19
        ).grid(row=rowNum, column=0, sticky='nsew')
    tk.Label(frameTempHumid, 
        text=dataList[1]
        ).grid(row=rowNum, column=1, sticky='nsew')
    tk.Label(frameTempHumid, 
        text=dataList[2]
        ).grid(row=rowNum, column=2, sticky='nsew')

if __name__ == '__main__':
    w = 1000
    h = 1080
    wRatio0 = 3
    wRatio1 = 7
    wRatioAll = wRatio0 + wRatio1
    hRatio0 = 1
    hRatio1 = 1
    hRatioAll = hRatio0 + hRatio1

    root = tk.Tk()
    root.geometry(str(w)+"x"+str(h)+"+0+0")
    root.title("kisobutu BBS")
    root.update()

    rw = root.winfo_width()
    rh = root.winfo_height()
    frameTempHumid = tk.Frame(root, bg = 'blue', width=rw*wRatio0/wRatioAll, height=rh*hRatio0/hRatioAll)
    frameCompletion = tk.Frame(root, bg = 'red', width=rw*wRatio1/wRatioAll, height=rh*hRatio0/hRatioAll)
    frameCamera = tk.Frame(root, bg = 'green', width=rw, height=rh*hRatio1/hRatioAll)
    frameTempHumid.grid(row=0, column=0, sticky='nsew')
    frameCompletion.grid(row=0, column=1, sticky='nsew')
    frameCamera.grid(row=1, column=0, columnspan=2, sticky='nsew')

    root.grid_columnconfigure(0, weight=wRatio0)
    root.grid_columnconfigure(1, weight=wRatio1)
    root.grid_rowconfigure(0, weight=hRatio0)
    root.grid_rowconfigure(1, weight=hRatio1)

    frameTempHumid.update()
    print( frameTempHumid.winfo_width() )


    pathSeonsorData = "C:/kisobutu/bbs/sensors/data/"
    # pathSeonsorData = "/home/pi/kisobutu/bbs/sensors/data/"
    sensorsFilename = "sensors.json"
    path = pathSeonsorData + sensorsFilename

    sensorsStr = ""
    with open(path) as f:
        s = f.read()
        print(type(s))
        print(s)
        sensorsStr = s

    print(sensorsStr)
    sensorsDic = json.loads(sensorsStr)
    print(sensorsDic["time"])
    print(sensorsDic["humidity"])
    print(sensorsDic["temperature"])

    

    addTempHumidGrid(0, ["場所", "温度", "湿度"])
    addTempHumidGrid(1, ["ラズパイ1", sensorsDic["temperature"], sensorsDic["humidity"]])
    # addTempHumidGrid(2, ["西", "20.0", "54.2"])

    root.mainloop()
