#include <M5StickC.h>
#include "WiFi.h"
#include <WebServer.h>

#include "DHT12.h"
#include <Wire.h>
#include "Adafruit_Sensor.h"
#include <Adafruit_BMP280.h>
DHT12 dht12;
Adafruit_BMP280 bme;
uint8_t setup_flag = 1;

#define BTN_A_PIN  37
#define BTN_ON  LOW
#define BTN_OFF HIGH
uint8_t prev_btn_a = BTN_OFF;
uint8_t btn_a      = BTN_OFF;
#define DISP_BRIGHTNESS_MIN  6 // 0〜6もセット可能だが、何も見えない
#define DISP_BRIGHTNESS_MAX 12 //
uint8_t disp_brightness = DISP_BRIGHTNESS_MIN;
double vbat = 0.0;
int8_t bat_charge_p = 0;

IPAddress ipDetected;
const char* ssid = "ssid";
const char* pw = "pw";
IPAddress ip(192,168,100,150);
IPAddress subnet(255,255,255,0);
IPAddress gateway(192,168,100,102);
IPAddress DNS(192,168,100,102);


WebServer server(80);
String dataStr;
String temperatureStr;
String humidityStr;
String pressureStr;


void setup() {
  Serial.begin(115200);


  M5.begin();
  Wire.begin(0,26);
  //M5.Lcd.setTextSize(2);
  M5.Lcd.setRotation(3);
  M5.Lcd.fillScreen(BLACK);
//  pinMode(M5_BUTTON_HOME, INPUT);
  pinMode(BTN_A_PIN,  INPUT_PULLUP);
  M5.Axp.ScreenBreath(disp_brightness+2); // 液晶の明るさ設定


  M5.Lcd.setCursor(0, 0, 2);
  M5.Lcd.println("Connecting...");

  // start connecting a specific wifi
  Serial.println("Start WiFi setting: ");
  WiFi.config(ip, gateway, subnet, DNS);
  WiFi.begin(ssid, pw);

  // check the connection 
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);

    // check time out for break;
    int limitTime = 10000;
    if ( limitTime < millis() ) {
      Serial.println("break");
      break;
    }
  }

  if (WiFi.status() == WL_CONNECTED) {
    Serial.println("");
    Serial.println(WiFi.localIP());
  }

  server.on("/", handleRoot);
  server.on("/data", getData);
  server.on("/temperature", getTemperature);
  server.on("/humidity", getHumidity);
  server.on("/pressure", getPressure);
  server.onNotFound(handleNotFound);
  server.begin();
  Serial.println("HTTP server started");


  // Initialization #############################################
  // Pressure ===================================================
  if (!bme.begin(0x76)){  
      Serial.println("Could not find a valid BMP280 sensor, check wiring!");
      while (1);
  }

  // Display ====================================================
  Serial.print("\n\rCalibrate done..");
}


void loop() {
  // check connection and display it
  M5.Lcd.setCursor(0, 0, 2);
  if(WiFi.status() == WL_CONNECTED) {
    ipDetected = WiFi.localIP();
    M5.Lcd.println(ipDetected);
  }
  else {
    M5.Lcd.println("No connetion");
  }

  
  show_battery_info();

  btn_a = digitalRead(BTN_A_PIN);

  if(prev_btn_a == BTN_OFF && btn_a == BTN_ON){
    // ボタンAが押されたとき。1回ごとにディスプレイの明るさを上げる。
    // 最小7、最大12。12を超えたら0に戻す
    disp_brightness += 1;
    if(disp_brightness > DISP_BRIGHTNESS_MAX){
      disp_brightness = DISP_BRIGHTNESS_MIN;
    }
    M5.Axp.ScreenBreath(disp_brightness);
    M5.Lcd.setCursor(135, 20);
//    M5.Lcd.printf("Brightness:%2d", disp_brightness);
    M5.Lcd.printf("%2d", disp_brightness);
  }

  prev_btn_a = btn_a;


  
  // Temperature ##################################################
  float tmp = dht12.readTemperature();
  temperatureStr = String(tmp);
  M5.Lcd.setCursor(0, 20, 2);
  M5.Lcd.printf("Temp: %04.1f dC", tmp);

  // Humidity ###################################################
  float hum = dht12.readHumidity();
  humidityStr = String(hum);
  M5.Lcd.setCursor(0, 40, 2);
  M5.Lcd.printf("Humi: %04.1f %%", hum);
  
  // Pressure ###################################################
  float pressure = bme.readPressure() / 100.0;
  pressureStr = String(pressure);
  M5.Lcd.setCursor(0, 60, 2);
  M5.Lcd.printf("Pressure: %04.1f hPa", pressure);
  delay(100);

  dataStr = "{_temperature_:_"+temperatureStr+"_, _humidity_:_"+humidityStr+"_, _pressure_:_"+pressureStr+"_}";


  // Calibration ################################################
  if(!setup_flag){
    setup_flag = 1;
    
    // Pressure =================================================
    if (!bme.begin(0x76)){  
      Serial.println("Could not find a valid BMP280 sensor, check wiring!");
      while (1);
    }
    
    // Display ==================================================
    Serial.print("\n\rCalibrate done..");
  }

//  if(digitalRead(M5_BUTTON_HOME) == LOW){
//    setup_flag = 0;
//    while(digitalRead(M5_BUTTON_HOME) == LOW);
//  }

  server.handleClient();

}



void show_battery_info(){
  // バッテリー電圧表示
  // GetVbatData()の戻り値はバッテリー電圧のステップ数で、
  // AXP192のデータシートによると1ステップは1.1mV
  vbat = M5.Axp.GetVbatData() * 1.1 / 1000;
  //M5.Lcd.setCursor(0, 5);
  //M5.Lcd.printf("Volt: %.2fV", vbat);

  // バッテリー残量表示
  // 簡易的に、線形で4.2Vで100%、3.0Vで0%とする
  bat_charge_p = int8_t((vbat - 3.0) / 1.2 * 100);
  if(bat_charge_p > 100){
    bat_charge_p = 100;
  }else if(bat_charge_p < 0){
    bat_charge_p = 0;
  }
  //M5.Lcd.setCursor(120, 0);
  M5.Lcd.setCursor(120, 0, 2);
  //M5.Lcd.printf("Charge: %3d%%", bat_charge_p);
  M5.Lcd.printf("%3d%%", bat_charge_p);

  // 液晶の明るさ表示
  //M5.Lcd.setCursor(0, 45);
  //M5.Lcd.printf("Brightness:%2d", disp_brightness);
}



void handleRoot() {
  char temp[400];

  snprintf(temp, 400,
  "<html>\
    <head>\
      <meta http-equiv='Content-Type' content='text/html'/>\
      <title>ESP32 Demo</title>\
      <style>\
      body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
      </style>\
    </head>\
    <body>\
      <h1>Hello from ESP32!</h1>\
      <p>Hello !!</p>\
    </body>\
  </html>"
  );
  
  server.send(200, "text/html", temp);
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";

  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }

  server.send(404, "text/plain", message);
}

void getData() {
  server.send(200, "text/plain", dataStr);
}

void getTemperature() {
  server.send(200, "text/plain", temperatureStr);
}

void getHumidity() {
  server.send(200, "text/plain", humidityStr);
}
void getPressure() {
  server.send(200, "text/plain", pressureStr);
}
